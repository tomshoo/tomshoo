# Shubhanshu Tomar

## Hi there 👋

I am Shubhanshu Tomar, a final year student at IKGPTU,
Kapurthala, Pubjab, India.

I like to work and tinker along with computers, to learn
and grow from them. I believe in acquiring knowledge through
experimentation, and trials. I love designing system architectures,
and have fun understanding how computers "talk".

I am a huge fan of Star-Wars, and love playing video games,
as such fictional media of entertainment allow us to look
at our own world in much more creative and exciting ways. For
without creativity and imagination are we even human?
